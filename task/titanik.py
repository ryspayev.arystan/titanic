import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    
    median_values = df.groupby(df['Name'].str.extract(r'(\w+)\.')[0])['Age'].median().to_dict()

    
    results = []

    for title in ['Mr.', 'Mrs.', 'Miss.']:
        num_missing = df[(df['Name'].str.contains(title)) & (df['Age'].isnull())].shape[0]
        median_age = median_values.get(title, 0)
        results.append((title, num_missing, round(median_age)))

    return results